document.addEventListener('DOMContentLoaded', function() {
  document.getElementById('text1').style.display = 'block';
})

let elem = document.getElementById('list').addEventListener('click', function(event) {
  
    if (event.target.tagName === 'LI') {
   
      const selectedId = event.target.id;
  
      const par = document.getElementById('par').children;
      for (let i = 0; i < par.length; i++) {
        par[i].style.display = 'none';
      }
  
      document.getElementById('text' + selectedId.slice(-1)).style.display = 'block';
    }
  });




  
  const tabs = document.querySelectorAll('.tabs-title');

    tabs.forEach(tab => {
        tab.addEventListener('click', function () {
            tabs.forEach(t => t.classList.remove('active'));
            this.classList.add('active');
        });
    });



    const tips = document.querySelectorAll('.more-tabs-title');

    tips.forEach(tab => {
        tab.addEventListener('click', function () {
            tips.forEach(t => t.classList.remove('active'));
            this.classList.add('active');
        });
    });


    document.addEventListener('DOMContentLoaded', function () {
      const picturesContainer = document.getElementById('pictures');
      const loadMoreButt = document.querySelector('.load-more');
  
      const additionalImages = [
          'images/graphic-design6.jpg',
          'images/graphic-design10.jpg',
          'images/graphic-design9.jpg',
          'images/graphic-design11.jpg',
          'images/graphic-design7.jpg',
          'images/graphic-design3.jpg',
          'images/graphic-design1.jpg',
          'images/graphic-design2.jpg',
          'images/graphic-design12.jpg',
          'images/graphic-design10.jpg',
          'images/graphic-design4.jpg',
          'images/graphic-design8.jpg',
      ];
  
      function loadMoreImages() {
          loadMoreButt.disabled = true;
          loadMoreButt.textContent = 'Loading...';
  
          setTimeout(function () {
              for (let i = 0; i < 12; i++) {
                  const img = document.createElement('img');
                  img.src = additionalImages[i % additionalImages.length];
                  const li = document.createElement('li');
                  li.classList.add('pic');
                  li.appendChild(img);
                  picturesContainer.appendChild(li);
              }
  
              
              loadMoreButt.style.display = 'none';
          }, 2000);
      };
  
      loadMoreButt.addEventListener('click', loadMoreImages);
  });


  





  document.addEventListener('DOMContentLoaded', function () {
    const tips = document.querySelectorAll('.more-tabs-title');
    const pictures = document.querySelectorAll('.photo li[data-category]');

    tips.forEach(tab => {
        tab.addEventListener('click', function () {
            const category = this.getAttribute('data-category');

            
            if (category === 'all') {
                pictures.forEach(picture => {
                    picture.classList.remove('hidden');
                });
            } else {
                
                pictures.forEach(picture => {
                    const pictureCategory = picture.getAttribute('data-category');
                    if (pictureCategory === category) {
                        picture.classList.remove('hidden');
                    } else {
                        picture.classList.add('hidden');
                    }
                });
            }

            
            tips.forEach(tab => tips.classList.remove('active'));
            this.classList.add('active');
        });
    });
});





document.addEventListener('DOMContentLoaded', function () {
    const imgMenuItems = document.querySelectorAll('.choose-img');
    const allPersonElements = document.querySelectorAll('.person .each');
    let currentIndex = 0;
    let selectedHuman = null; 

    function showPerson(index) {
        allPersonElements.forEach(personElement => {
            personElement.style.display = 'none';
        });

        const allTextElements = document.querySelectorAll('.about-person li');
        allTextElements.forEach(textElement => {
            textElement.style.display = 'none';
        });

        const selectedPerson = document.getElementById(`pers${index + 1}`);
        const targetText = document.getElementById(`ab${index + 1}`);
        const human = document.getElementById(`people${index + 1}`);

        if (selectedPerson && targetText && human) {
            selectedPerson.style.display = 'block';
            targetText.style.display = 'block';

        
            if (selectedHuman) {
                selectedHuman.style.border = 'none';
            }

            human.style.border = '5px solid #1FDAB5';
            human.style.borderRadius = '180px';

            selectedHuman = human;
        }
    }

    function updateIndex(offset) {
        currentIndex = (currentIndex + offset + imgMenuItems.length) % imgMenuItems.length;
    }

    imgMenuItems.forEach((item, index) => {
        item.addEventListener('click', function () {
            imgMenuItems.forEach(img => img.classList.remove('raised'));
            this.classList.add('raised');

            currentIndex = index;
            showPerson(currentIndex);
        });
    });

    document.querySelector('.square-arrow-l').addEventListener('click', function () {
        updateIndex(-1);
        showPerson(currentIndex);
    });

    document.querySelector('.square-arrow-r').addEventListener('click', function () {
        updateIndex(1);
        showPerson(currentIndex);
    });

    showPerson(currentIndex);
});








document.addEventListener('DOMContentLoaded', function () {
    const picturesContainer = document.getElementById('img-block');
    const loadMoreButton = document.querySelector('.load-more-btn');

    const additionalImages = [
        'images/graphic-design6.jpg',
        'images/graphic-design10.jpg',
        'images/graphic-design9.jpg',
        'images/graphic-design11.jpg',
        'images/graphic-design7.jpg',
        'images/graphic-design3.jpg',
        'images/last-block/ringve-museum-1 1.png',
        'images/last-block/ringve-museum-1 1.png',
        'images/last-block/ringve-museum-1 1.png',
        'images/last-block/vanglo-house-6 1.png',
        'images/last-block/vanglo-house-1 1 (1).png ',
        'images/last-block/vanglo-house-6 1.png ',
    ];

    function loadMoreImages() {
        loadMoreButton.disabled = true;
        loadMoreButton.textContent = 'Loading...';

        setTimeout(function () {
            for (let i = 0; i < 12; i++) {
                const img = document.createElement('img');
                img.src = additionalImages[i % additionalImages.length];
                const li = document.createElement('li');
                li.classList.add('pic');
                li.appendChild(img);
                picturesContainer.appendChild(li);
            }

            loadMoreButton.style.display = 'none';
        }, 2000);
    };

    loadMoreButton.addEventListener('click', loadMoreImages);
});





document.addEventListener('DOMContentLoaded', function () {
  const landingItem = document.querySelector('[data-category="landing"]');
  const createItem = document.querySelector('.create');

  landingItem.addEventListener('mouseover', function () {
    createItem.classList.add('active');
  });

  landingItem.addEventListener('mouseout', function () {
    createItem.classList.remove('active');
  });
});




//   document.addEventListener('DOMContentLoaded', function () {
//     let el = document.getElementById('photocat').addEventListener('click', function (event) {
//         const selectedId = event.target.id;
//         const pics = document.querySelectorAll('.pic');

//         // Приховувати всі картинки
//         pics.forEach(pic => {
//             pic.classList.add('hidden');
//         });

//         // Відображати лише перші три картинки для обраної вкладки
//         const selectedPics = document.querySelectorAll(`.pic.${selectedId}`);
//         selectedPics.forEach((pic, index) => {
//             if (index < 3) {
//                 pic.classList.remove('hidden');
//             }
//         });
//     });
// });

    // document.addEventListener('DOMContentLoaded', function() {
    //         let el = document.getElementById('photocat').addEventListener('click', function(event) {
    //             const selectedId = event.target.id;
    //             const pics = document.querySelectorAll('.pic');
    //             const pic1 = document.querySelectorAll('.pic1');
    //             const pic2 = document.querySelectorAll('.pic-pic');
    //             // При натисканні на вкладку "ALL" відображати всі картинки
    //             if (selectedId === 'all') {
    //                 pics.forEach(pic => {
    //                     pic.classList.remove('hidden');
    //                 });
    //             } else if (selectedId === 'word') {
    //                 // Приховувати всі картинки
    //                 pic1.forEach(pic1 => {
    //                     pic1.classList.add('hidden');
    //                 });
    //             } else if(selectedId === 'graph'){
    //                 pic2.forEach(pic2 => {
    //                     pic2.classList.add('hidden');
    //                 });
    //             }
    //         });
    //     });